How to use this:

### First "omake netamqp_methods_0_9.mli" to get this file!

Note that the term "synchronous" is used in AMQP for a
request/response pair, and "asynchronous" for a one-way message. Of
course, we process everything asynchronously (non-blocking)
internally, and this is made accessible via an engine-based interface
(suffix _e). There are also synchronous (blocking) variants (suffix
_s). Just for adding confusion.

1. Create endpoint:

let ep = Netamqp_endpoint.create
           (`TCP (`Inet("server", Netamqp_endpoint.default_port)))
           esys

2. Create connection object:

let conn = Netamqp_connection.create ep

3. Open connection object:

let auth = Netamqp_connection.plain_auth "user" "user's password"
Netamqp_connection.open_s conn `AMQP_0_9 [auth] (`Pref "en") "virtual_host"

- there is also an asynchronous open_e function

4. Open channel 1:

let chan_n = 1
let chan = Netamqp_channel.open_s conn chan_n

5. Call some synchronous methods:

- from here on it is quite low-level!

let m = `Queue_declare(0, "name", false, true, false, false, false, [])
let (ret_m, _) = Netamqp_endpoint.sync_c2s_s ep (`AMQP_0_9 m) None chan_n

ret_m is now `Queue_declare_ok (with some args)

See netamqp_methods_0_9.mli (generated file) for details. The 
amqp0_9_1.xml file contains documentation.

6. Publish something (async):

let m = `Basic_publish(0, "exchange", "routing-key", false, false)
let props = `P_basic("application/octet-stream", "US-ASCII", ...)
let data = (`AMQP_0_9 props, [Netamqp_rtypes.mk_mstring "some data stuff"])
Netamqp_endpoint.async_c2s ep (`AMQP_0_9 m) (Some data) chan_n

7. Get notifications (async):

Netamqp_endpoint.register_async_s2c
   ep `Basic_deliver chan_n (fun m -> ...)

The function is called back whenever a `Basic_deliver message arrives,
and m is this message.

8. Close:

Netamqp_channel.close_s chan;
Netamqp_connection.close_s conn

--


These were just examples how open and close connections, and how
to use the three importants way of sending and receiving methods:

- Synchronous: 5.
- Asynchronous send: 6.
- Asynchronous receive: 7

It is of course also required to use this in the right way. E.g.
one only gets notifications `Basic_deliver when this is requested
at the server (using `Basic_consume), and each delivered message
needs to be acknowledged (`Basic_ack).
